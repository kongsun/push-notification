package com.example.kongsun.pushnotification;

import android.app.Application;

import com.onesignal.OneSignal;

public class OneSignalApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert)
                .setNotificationOpenedHandler(new NotificationHandler(this))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
}

